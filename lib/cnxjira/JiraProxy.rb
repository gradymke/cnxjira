require 'rest-client'
require 'json'

MAX_USER_RETURN = 50

require_relative 'AtlassianUser'
require_relative 'JiraIssue'
require_relative 'JiraWorklog'

module CNXJira
  class JiraProxy

    def initialize(url, user, password)
      @url = url
      @user = user
      @password = URI::encode(password)
    end

    # Executes an API call. Returns the RAW text result
    # Examples of API calls:
    #  - search?jql={something}
    #  - group?groupname={something}&options
    def execute_api(api_call, method = :get)
      url = "#{@url}/#{api_call}"
      RestClient::Request.new(:method => method, :url => url, :user => @user, :password => @password, :headers => {'content-type' => 'application/json'}).execute
      # if method == :post
      #   puts "POST! Setting json type"
      #   res.headers[:content_type] = "application/json"
      # end
      # res.execute
    end

    # Takes a jql text and returns the JSON Object
    def self.jql(jira, query_text)
      query = URI::encode(query_text)
      api_call = "search?maxResults=1000&jql=#{query}"
      begin
        json_text = jira.execute_api(api_call)
        response_json = JSON.parse(json_text)

        issues = []
        response_json["issues"].each do |issue|
          issues << CNXJira::JiraIssue.fromJSON(issue)
        end

        return issues
      rescue
        # This swallows all bad requests - maybe bad?
        return []
      end
    end

    # Takes a KEY and returns the worklogs for it
    def self.worklog(jira, issue_key)
      api_call = "issue/#{issue_key}/worklog?maxResults=1000"
      json_text = jira.execute_api(api_call)
      response_json = JSON.parse(json_text)

      worklogs = []

      response_json["worklogs"].each do |worklog|
        worklogs << CNXJira::JiraWorklog.fromJSON(worklog, issue_key)
      end

      return worklogs
    end

    # Initiates a ReIndex on the site - REQUIRES ADMIN CREDENTIALS
    def self.reindex(jira, type = 'background')
      api_call = "reindex?type=#{type}"
      json_text = jira.execute_api(api_call, :post)

      return 0 # TODO Need to return a value here.
    end

    # Takes a groupname and returns the number of users in that gruop
    def self.userCountInGroup(jira, group_name)
      api_call = "group?groupname=#{group_name}"
      json_text = jira.execute_api(api_call)

      response_json = JSON.parse(json_text)
      response_json["users"]["size"].to_i
    end

    # Takes a groupname, starting index and length and returns an array of AtlassianUser objects
    def self.usersInGroupRange(jira, group_name, start_idx, end_idx)
      api_call = "group?groupname=#{group_name}&expand=users[#{start_idx}:#{end_idx}]"
      json_text = jira.execute_api(api_call)

      response_json = JSON.parse(json_text)

      users = []

      response_json["users"]["items"].each do |user|
        users << CNXJira::AtlassianUser.new(user["name"], user["emailAddress"])
      end

      users
    end

    def self.usersInGroup(jira, group_name)
      # See how many users are in this group
      num_users = self.userCountInGroup(jira, group_name)

      group_users = []

      # Since we can only get 50 users at a time, loop through to get all of the users.
      start_user = 0
      while start_user < num_users
        # Determine the end user
        end_user = start_user + MAX_USER_RETURN - 1
        if end_user > num_users - 1
          end_user = num_users - 1
        end

        # Call the API and add these users to the group
        group_users.concat(self.usersInGroupRange(jira, group_name, start_user, end_user))

        # Increment the starting user
        start_user += MAX_USER_RETURN
      end

      group_users
    end
  end
end
