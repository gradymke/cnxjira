# Assumptions of this class:
# * For TYPE, we use the NAME, not the ID

# COMPONENT is deprecated and will be removed in 0.6.0

module CNXJira
  class JiraIssue
    attr_accessor :id, :key, :component, :components, :summary, :type, :priority, :labels, :project, :story_points, :subtasks, :subtask, :resolved, :fluid_state, :epic_name

    def initialize (id = 0, key = "")
      @id = id
      @key = key
      @components = []
      @subtasks = []
    end

    def ==(another_issue)
      self.key == another_issue.key
    end

    def eql?(another_issue)
      self == another_issue
    end

    def hash
      self.key.hash
    end

    def to_s
      "#{@key}"
    end

    # Static methods

    # This method is used to create this object given the resulting JSON from a
    # REST call
    def self.fromJSON(jsonIssue)
      # For now, we'll only use the FIRST component on an issue.
      component = jsonIssue["fields"]["components"].any? ? jsonIssue["fields"]["components"][0]["name"] : ""
      summary = jsonIssue["fields"]["summary"]
      type = jsonIssue["fields"]["issuetype"]["name"]
      project = jsonIssue["fields"]["project"]["key"]

      issue = JiraIssue.new(jsonIssue["id"], jsonIssue["key"])
      issue.component = jsonIssue["fields"]["components"].any? ? jsonIssue["fields"]["components"][0]["name"] : ""
      issue.summary = jsonIssue["fields"]["summary"]
      issue.type = jsonIssue["fields"]["issuetype"]["name"]
      issue.project = jsonIssue["fields"]["project"]["key"]
      issue.priority = jsonIssue["fields"]["priority"] ? jsonIssue["fields"]["priority"]["name"] : ""
      issue.labels = jsonIssue["fields"]["labels"]

      issue.story_points = jsonIssue["fields"]["customfield_10004"] ? jsonIssue["fields"]["customfield_10004"] : 0
      issue.fluid_state = jsonIssue["fields"]["customfield_10100"] ? jsonIssue["fields"]["customfield_10100"]["value"] : "None"
      issue.epic_name = jsonIssue["fields"]["customfield_10009"] ? jsonIssue["fields"]["customfield_10009"] : ""

      # Add support for "components"
      if jsonIssue["fields"]["components"].any?
        issue.components = jsonIssue["fields"]["components"].map do |component|
          component["name"]
        end
      end

      # Determine if this issue is a subtask or not
      issue.subtask = jsonIssue["fields"]["issuetype"]["subtask"]

      # Determine if this issue is resolved or not
      issue.resolved = (!jsonIssue["fields"]["resolution"].nil? && jsonIssue["fields"]["resolution"].any?)

      # Look for subtasks
      if jsonIssue["fields"]["subtasks"].any?
        issue.subtasks = jsonIssue["fields"]["subtasks"].map do |subtask|
          subtask["key"]
        end
      end

      return issue
    end
  end
end
