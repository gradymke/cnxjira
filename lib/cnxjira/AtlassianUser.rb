module CNXJira
  class AtlassianUser
    attr_accessor :name, :email, :confluence, :jira

    def initialize (name, email = "", confluence = false, jira = false)
      @name = name;
      @email = email
      @confluence = confluence
      @jira = jira
    end

    def ==(another_user)
      self.name == another_user.name
    end

    def eql?(another_user)
      self == another_user
    end

    def hash
      self.name.hash
    end

    # Static methods

    # This method is used to create this object given the resulting JSON from a
    # REST call
    def self.fromJSON(jsonUser)
      return AtlassianUser.new(jsonUser["name"], jsonUser["emailAddress"])
    end
  end
end
