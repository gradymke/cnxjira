# TODO - at some point this should be a CHILD of the Issue and have a
# method off of it to retrieve - for now we'll leave it like this, however.

module CNXJira
  class JiraWorklog
    attr_accessor :id, :key, :name, :work_date, :time_spent_seconds

    def initialize (id = 0, key = "", name = "")
      @id = id
      @key = key
      @name = name
      @time_spent_seconds = 0
    end

    def time_spent_hours
      @time_spent_seconds / 60.0 / 60.0
    end

    def ==(another_worklog)
      self.id == another_worklog.id
    end

    def eql?(another_worklog)
      self == another_worklog
    end

    def hash
      self.id.hash
    end

    def to_s
      "#{@key} - #{@name} - #{@work_date} - #{@time_spent_seconds}"
    end

    # Static methods

    # This method is used to create this object given the resulting JSON from a
    # REST call
    def self.fromJSON(jsonWorklog, key)
      # For now, we'll only use the FIRST component on an issue.
      name = jsonWorklog["author"]["name"]
      work_date = Date.parse(jsonWorklog["started"])
      time_spent_seconds = jsonWorklog["timeSpentSeconds"]

      worklog = JiraWorklog.new(jsonWorklog["id"], key, name)
      worklog.work_date = work_date
      worklog.time_spent_seconds = time_spent_seconds

      return worklog
    end
  end
end
