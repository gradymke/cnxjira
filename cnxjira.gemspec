# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'cnxjira/version'

Gem::Specification.new do |spec|
  spec.name          = "cnxjira"
  spec.version       = CNXJira::VERSION
  spec.authors       = ["Brandon Grady"]
  spec.email         = ["brandon@dogsbuttbrew.com"]
  spec.summary       = %q{Gem for interacting with JIRA for CNX projects.}
  spec.description   = %q{Gem for interacting with JIRA for CNX projects. This is used for internal CNX projects mainly.}
  spec.homepage      = "https://bitbucket.org/gradymke/cnxjira"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake", "~> 10.1"
  spec.add_development_dependency "rspec", "~> 3.1"

  spec.add_runtime_dependency "rest-client", "~> 1.6"
  spec.add_runtime_dependency "json", "~> 1.8"
end
