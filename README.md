# Cnxjira

This GEM was created to assist in communicating with Atlassian's OnDemand
instance of JIRA. It is used primarily in Connecture projects.

## Installation

Add this line to your application's Gemfile:

    gem 'cnxjira', :git => "https://bitbucket.org/gradymke/cnxjira.git"

And then execute:

    $ bundle

As it is inly available via git source at this time, to install via the "gem"
utility, you must first download the source, then build the gem prior to
installing:

    $ git clone https://bitbucket.org/gradymke/cnxjira.git cnxjira
    $ cd cnxjira
    $ gem build cnxjira.gemspec
    $ gem install cnxjira-0.0.1.gem

## Usage

To create a JIRA connection, do the following:

    jira = CNXJira::JiraProxy.new("https://jiraconn.atlassian.net/rest/api/2", "jiraUser", "jiraPassword")

To issue a _jql_ query:

    query = "key in workedIssues(-2w, 0m, #{user})"
    jira_issues = CNXJira::JiraProxy.jql(jira, query)

Note the instantiated "jira" object is passed to the static query method and
the result is an array of **JiraIssue** objects.

## Object Documentation

_**TODO**_ Create this documentation.

* JiraProxy
* JiraIssue
* AtlassianUser
* JiraWorklog

## Contributing

Feel free to create a pull-request for adding to this codebase.

## History

* 0.0.3 - Fixed issue with eqality checking in JiraIssue, JiraWorklog, and AtlassianUser objects.
* 0.0.4 - Swallow bad requests resulting from JQL and just return an empty issues list.
* 0.0.5 - Added "components" accessor to the JiraIssue object. Will be deprecating "component" in the next version.
* 0.0.6 - Added "subtasks" accessor to the JiraIssue object.
* 0.0.7 - Added "subtask" property.
* 0.0.8 - Fixed bug with subtasks not existing.
* 0.0.9 - Fixed another bug with subtasks not actually working properly.
* 0.0.10 - Created the "reindex" call on the JiraProxy object to reindex a system.
* 0.0.11 - Updated to make tests work again. Added "resolved" flag to an issue. Added "fluid_state" to the issue.
* 0.0.12 - Added epic_name.
