require "spec_helper"

describe CNXJira::JiraWorklog do
  before do
    json_text =<<eos

{"self":"https://connecture.atlassian.net/rest/api/2/issue/77030/worklog/38643","author":{"self":"https://connecture.atlassian.net/rest/api/2/user?username=tcvetan","name":"tcvetan","emailAddress":"tcvetan@connecture.com","avatarUrls":{"16x16":"https://secure.gravatar.com/avatar/26b29ed18cb05bad77718e0d08ad3cd6?d=mm&s=16","24x24":"https://secure.gravatar.com/avatar/26b29ed18cb05bad77718e0d08ad3cd6?d=mm&s=24","32x32":"https://secure.gravatar.com/avatar/26b29ed18cb05bad77718e0d08ad3cd6?d=mm&s=32","48x48":"https://secure.gravatar.com/avatar/26b29ed18cb05bad77718e0d08ad3cd6?d=mm&s=48"},"displayName":"Tyler Cvetan","active":true},"updateAuthor":{"self":"https://connecture.atlassian.net/rest/api/2/user?username=tcvetan","name":"tcvetan","emailAddress":"tcvetan@connecture.com","avatarUrls":{"16x16":"https://secure.gravatar.com/avatar/26b29ed18cb05bad77718e0d08ad3cd6?d=mm&s=16","24x24":"https://secure.gravatar.com/avatar/26b29ed18cb05bad77718e0d08ad3cd6?d=mm&s=24","32x32":"https://secure.gravatar.com/avatar/26b29ed18cb05bad77718e0d08ad3cd6?d=mm&s=32","48x48":"https://secure.gravatar.com/avatar/26b29ed18cb05bad77718e0d08ad3cd6?d=mm&s=48"},"displayName":"Tyler Cvetan","active":true},"comment":"UGH - Help Jeff","created":"2014-02-04T15:55:53.851-0600","updated":"2014-02-04T15:55:53.851-0600","started":"2014-02-04T15:55:00.000-0600","timeSpent":"1h","timeSpentSeconds":3600,"id":"38643"}
eos
    response_json = JSON.parse(json_text)
    @jira_worklog = CNXJira::JiraWorklog.fromJSON(response_json, "TIME-11")
  end

  subject { @jira_worklog }

  describe "should have created the object based on the JSON properly" do
    specify do
      @jira_worklog.key.should eq("TIME-11")
    end
  end

  describe "time values" do
    describe "should return the right seconds" do
      specify { @jira_worklog.time_spent_seconds.should eq(3600) }
    end
    describe "should return the right hours" do
      specify { @jira_worklog.time_spent_hours.should eq(1) }
    end
    describe "should return floating point hours" do
      specify do
        tmp_worklog = @jira_worklog.dup
        tmp_worklog.time_spent_seconds = 5400
        tmp_worklog.time_spent_hours.should eq(1.5)
      end
    end
  end

  describe "equality" do
    before do
      @new_worklog = CNXJira::JiraWorklog.new(@jira_worklog.id, @jira_worklog.key, @jira_worklog.name)
    end
    describe "== is true when it has the same id" do
      specify { @new_worklog.should eq(@jira_worklog) }
    end
    describe ".eql? is true when it has the same id" do
      specify { @new_worklog.should eql(@jira_worklog) }
    end
    describe ".hash is the same when it has the same id" do
      specify { @new_worklog.hash.should eql(@jira_worklog.hash) }
    end
  end

end
