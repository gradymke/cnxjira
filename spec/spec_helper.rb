require 'bundler/setup'
Bundler.setup

require 'rspec'
require 'cnxjira'
require 'rest-client'
require 'json'

RSpec.configure do |config|
  # some (optional) config here
end
