require "spec_helper"

describe CNXJira::JiraProxy do
  before do
    @jira = CNXJira::JiraProxy.new("https://connecture.atlassian.net/rest/api/2", "api", "connect")
  end

  describe "good jql request" do
    specify do
      query = "key = TIME-1"
      issues = CNXJira::JiraProxy.jql(@jira, query)
      issues.count.should eq(1)
    end
  end

  describe "bad jql request" do
    specify do
      query = "key in (2014-05-05,2014-05-11,dumbuser)"
      issues = CNXJira::JiraProxy.jql(@jira, query)
      issues.count.should eq(0)
    end
  end
end
