require "spec_helper"

describe CNXJira::AtlassianUser do
  before do
    json_text =<<eos
{"self":"https://connecture.atlassian.net/rest/api/2/user?username=bgrady","key":"bgrady","name":"bgrady","emailAddress":"bgrady@connecture.com","avatarUrls":{"16x16":"https://secure.gravatar.com/avatar/71bde94207afcb4e8d09dcc50150ad65?d=mm&s=16","24x24":"https://secure.gravatar.com/avatar/71bde94207afcb4e8d09dcc50150ad65?d=mm&s=24","32x32":"https://secure.gravatar.com/avatar/71bde94207afcb4e8d09dcc50150ad65?d=mm&s=32","48x48":"https://secure.gravatar.com/avatar/71bde94207afcb4e8d09dcc50150ad65?d=mm&s=48"},"displayName":"Brandon Grady","active":true,"timeZone":"America/Chicago","groups":{"size":20,"items":[]},"expand":"groups"}
eos
    response_json = JSON.parse(json_text)
    @user = CNXJira::AtlassianUser.fromJSON(response_json)
  end

  subject { @user }

  describe "should have created the object based on the JSON properly" do
    specify do
      @user.name.should eq("bgrady")
      @user.email.should eq("bgrady@connecture.com")
    end
  end

  describe "equality" do
    before do
      @new_user = CNXJira::AtlassianUser.new("bgrady", "bgrady@connecture.com")
    end
    describe "== is true when it has the same name" do
      specify { @new_user.should eq(@user) }
    end
    describe ".eql? is true when it has the same name" do
      specify { @new_user.should eql(@user) }
    end
    describe ".hash is the same when it has the same name" do
      specify { @new_user.hash.should eql(@user.hash) }
    end
  end

end
